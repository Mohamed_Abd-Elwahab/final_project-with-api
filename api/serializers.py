from rest_framework import serializers
from articles.models import Article
from django.contrib.auth import get_user_model
class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ("title", "body", "date", "author")
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ("id", "username")
