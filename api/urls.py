from django.urls import path
from .views import ArticleApiView, DetailApiView

urlpatterns = [
    path("", ArticleApiView.as_view()),
    path("<int:pk>/", DetailApiView.as_view()),
]
